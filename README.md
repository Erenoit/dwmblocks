# Erenoit's dwmblocks
This is my fork of dwmblocks which i use with [my dwm](https://gitlab.com/Erenoit/dwm). The original repo is [here](https://github.com/torrinfail/dwmblocks).

## What is dwmblocks?
Modular status bar for dwm written in c.

## Requirements
In order to build dwmblocks you need the C header files.
And you need [my scripts](https://gitlab.com/Erenoit/scripts) in your `$PATH`.

## Installation
Enter the following command to build and install dwmblocks (if necessary as root):

```sh
make clean install
```

## Configuration
The configuration of dwmblocks is done by creating a custom config.h and (re)compiling the source code.


## Patches 
All the patches are included in `patches/` folder.
- [statuscmd](https://dwm.suckless.org/patches/statuscmd/)
