//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/
	{"", "sb_crypto_coin XMR usd --color", 3600,   7},
	{"", "sb_temperature --color",         20,     6},
	{"", "sb_cpu_usage --color",           20,     5},
	{"", "sb_memory_usage --color",        20,     4},
	{"", "sb_volume --color",              0,      3},
	{"", "sb_battery --color",             30,     2},
	{"", "sb_dateNclock jp --color",       60,     1},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 5;
